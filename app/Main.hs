module Main where

import Snap
import App

main :: IO ()
main = serveSnaplet defaultConfig app
