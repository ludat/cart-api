module App (app, routes) where

import Control.Lens
import Data.ByteString (ByteString)
import Snap

data App = App

makeLenses ''App

app :: SnapletInit b App
app = makeSnaplet "app" "Simple shopping cart" Nothing $ do
  addRoutes routes
  return App

routes :: [(ByteString, Handler b App ())]
routes = [("hi", writeBS "thing")]
