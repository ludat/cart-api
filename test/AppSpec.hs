module AppSpec (spec) where

import Snap (route)
import Test.Hspec.Snap
import Test.Hspec

import App (routes, app)

spec :: Spec
spec = snap (route routes) app $ do
       describe "a number" $ do
         it "ok" $ do
           r <- get "/"
           should200 r
